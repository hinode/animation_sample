package jp.itnav.hinode.animation_sample_1;

import android.app.Activity;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.BounceInterpolator;
import android.view.animation.RotateAnimation;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.ImageView;

/**
 * Created by itnav on 2015/02/19.
 */
public class Page_5 extends Activity {

    ImageView imageView;
    ImageView imageView1;
    MediaPlayer mediaPlayer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.page_5);
        imageView1 = (ImageView)findViewById(R.id.jet);
        imageView1.setVisibility(View.INVISIBLE);

        imageView = (ImageView)findViewById(R.id.charactor);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startAnim();


            }
        });

        Button button = (Button)findViewById(R.id.next);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Page_5.this, Page_6.class);
                startActivity(intent);
            }
        });

    }
    private void startAnim() {
        mediaPlayer = MediaPlayer.create(this, R.raw.nori_ge_roketo01);
        mediaPlayer.start();
        mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                mediaPlayer.release();
            }
        });


        TranslateAnimation translateAnimation1 = new TranslateAnimation(0, 10, 0, 5);
        translateAnimation1.setDuration(30);
        translateAnimation1.setRepeatMode(Animation.REVERSE);
        translateAnimation1.setRepeatCount(50);
        imageView.startAnimation(translateAnimation1);
        translateAnimation1.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                imageView1.setVisibility(View.VISIBLE);

                AnimationSet set = new AnimationSet(true);
                AnimationSet set1 = new AnimationSet(true);

                TranslateAnimation translateAnimation = new TranslateAnimation(0, 2, 0, -1500);
                translateAnimation.setDuration(2500);
                final RotateAnimation rotateAnimation = new RotateAnimation(0, -180, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, -3.5f);
                rotateAnimation.setDuration(300);
                rotateAnimation.setStartOffset(2500);

                TranslateAnimation translateAnimation2 = new TranslateAnimation(0, -70, 0, 770);
                translateAnimation2.setDuration(2500);
                translateAnimation2.setStartOffset(2800);
                translateAnimation2.setInterpolator(new BounceInterpolator());

                TranslateAnimation translateAnimation3 = new TranslateAnimation(0, 10, 0, 0);
                translateAnimation3.setDuration(25);
                translateAnimation3.setRepeatMode(Animation.REVERSE);
                translateAnimation3.setRepeatCount(100);

                RotateAnimation rotateAnimation1 = new RotateAnimation(0, 90, 20, 160);
                rotateAnimation1.setDuration(2000);
                rotateAnimation1.setStartOffset(5500);

                TranslateAnimation translateJet = new TranslateAnimation(0, 2, 0, -1500);
                translateJet.setDuration(2500);

                AlphaAnimation alphaAnimation = new AlphaAnimation(1, 0);
                alphaAnimation.setDuration(2500);

                set.addAnimation(translateAnimation);
                set.addAnimation(rotateAnimation);
                set.addAnimation(translateAnimation2);
                set.addAnimation(translateAnimation3);
                set.addAnimation(rotateAnimation1);
                set.setFillAfter(true);
                set.setFillEnabled(true);
                imageView.startAnimation(set);

                set1.addAnimation(translateJet);
                set1.addAnimation(translateAnimation3);
                set1.addAnimation(alphaAnimation);
                set1.setFillAfter(true);
                set1.setFillEnabled(true);
                imageView1.startAnimation(set1);

                translateAnimation3.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {

                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });
            }


            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }
}
