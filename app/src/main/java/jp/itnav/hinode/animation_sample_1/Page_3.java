package jp.itnav.hinode.animation_sample_1;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.LayoutAnimationController;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.ImageView;

/**
 * Created by itnav on 2015/02/19.
 */
public class Page_3 extends Activity implements Animation.AnimationListener{

    ImageView imageView;
    ImageView imageView2;
    ImageView imageView3;
    ImageView imageView4;
    ImageView imageView5;
    ImageView imageView6;
    ImageView imageView7;
    ImageView imageView8;

    Button button;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.page_3);



        imageView = (ImageView)findViewById(R.id.image1);
        imageView2 = (ImageView)findViewById(R.id.image2);
        imageView3 = (ImageView)findViewById(R.id.image3);
        imageView4 = (ImageView)findViewById(R.id.image4);
        imageView5 = (ImageView)findViewById(R.id.image5);
        imageView6 = (ImageView)findViewById(R.id.image6);
        imageView7 = (ImageView)findViewById(R.id.image7);
        imageView8 = (ImageView)findViewById(R.id.image8);

        button = (Button)findViewById(R.id.restart);
                 StartAnim();
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                StartAnim();
            }
        });



        Button button = (Button)findViewById(R.id.nextb);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Page_3.this, Page_4.class);
                startActivity(intent);
            }
        });
        
    }

    private void StartAnim() {


        imageView = (ImageView)findViewById(R.id.image1);
        imageView2 = (ImageView)findViewById(R.id.image2);
        imageView3 = (ImageView)findViewById(R.id.image3);
        imageView4 = (ImageView)findViewById(R.id.image4);
        imageView5 = (ImageView)findViewById(R.id.image5);
        imageView6 = (ImageView)findViewById(R.id.image6);
        imageView7 = (ImageView)findViewById(R.id.image7);
        imageView8 = (ImageView)findViewById(R.id.image8);

        TranslateAnimation translateAnimation = new TranslateAnimation(0, 0, 0, -80);
        translateAnimation.setDuration(1500);
        translateAnimation.setStartOffset(150);
        translateAnimation.setRepeatCount(-1);
        translateAnimation.setRepeatMode(Animation.REVERSE);
        imageView.startAnimation(translateAnimation);

        TranslateAnimation translateAnimation2 = new TranslateAnimation(0, 0, 0, -80);
        translateAnimation2.setDuration(1500);
        translateAnimation2.setStartOffset(200);
        translateAnimation2.setRepeatCount(-1);
        translateAnimation2.setRepeatMode(Animation.REVERSE);
        imageView2.startAnimation(translateAnimation2);

        TranslateAnimation translateAnimation3 = new TranslateAnimation(0, 0, 0, -80);
        translateAnimation3.setDuration(1500);
        translateAnimation3.setStartOffset(250);
        translateAnimation3.setRepeatCount(-1);
        translateAnimation3.setRepeatMode(Animation.REVERSE);
        imageView3.startAnimation(translateAnimation3);

        TranslateAnimation translateAnimation4 = new TranslateAnimation(0, 0, 0, -80);
        translateAnimation4.setDuration(1500);
        translateAnimation4.setStartOffset(300);
        translateAnimation4.setRepeatCount(-1);
        translateAnimation4.setRepeatMode(Animation.REVERSE);
        imageView4.startAnimation(translateAnimation4);

        TranslateAnimation translateAnimation5 = new TranslateAnimation(0, 0, 0, -80);
        translateAnimation5.setDuration(1500);
        translateAnimation5.setStartOffset(350);
        translateAnimation5.setRepeatCount(-1);
        translateAnimation5.setRepeatMode(Animation.REVERSE);
        imageView5.startAnimation(translateAnimation5);

        TranslateAnimation translateAnimation6 = new TranslateAnimation(0, 0, 0, -80);
        translateAnimation6.setDuration(1500);
        translateAnimation6.setStartOffset(400);
        translateAnimation6.setRepeatCount(-1);
        translateAnimation6.setRepeatMode(Animation.REVERSE);
        imageView6.startAnimation(translateAnimation6);

        TranslateAnimation translateAnimation7 = new TranslateAnimation(0, 0, 0, -80);
        translateAnimation7.setDuration(1500);
        translateAnimation7.setStartOffset(450);
        translateAnimation7.setRepeatCount(-1);
        translateAnimation7.setRepeatMode(Animation.REVERSE);
        imageView7.startAnimation(translateAnimation7);

        TranslateAnimation translateAnimation8 = new TranslateAnimation(0, 0, 0, -80);
        translateAnimation8.setDuration(1500);
        translateAnimation8.setStartOffset(500);
        translateAnimation8.setRepeatCount(-1);
        translateAnimation8.setRepeatMode(Animation.REVERSE);
        imageView8.startAnimation(translateAnimation8);

        LayoutAnimationController la = new LayoutAnimationController(translateAnimation);
        la.setDelay(0.5f);
    }

    @Override
    public void onAnimationStart(Animation animation) {

    }

    @Override
    public void onAnimationEnd(Animation animation) {

    }

    @Override
    public void onAnimationRepeat(Animation animation) {

    }
}
