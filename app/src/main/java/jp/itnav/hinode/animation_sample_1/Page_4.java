package jp.itnav.hinode.animation_sample_1;

import android.app.Activity;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.Button;
import android.widget.ImageView;

/**
 * Created by itnav on 2015/02/19.
 */
public class Page_4 extends Activity {

    ImageView imageView;

    MediaPlayer mediaPlayer;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.page_4);
        Button button = (Button)findViewById(R.id.next);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Page_4.this, Page_5.class);
                startActivity(intent);
            }
        });


        imageView = (ImageView)findViewById(R.id.charactor);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                starAnimation();
            }
        });
    }
    private void starAnimation() {
        RotateAnimation rotateAnimation = new RotateAnimation(0, 10, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        rotateAnimation.setDuration(5);
        rotateAnimation.setRepeatCount(80);
        rotateAnimation.setRepeatMode(Animation.REVERSE);
        imageView.startAnimation(rotateAnimation);

        mediaPlayer = MediaPlayer.create(this, R.raw.sei_ge_suzu06);
        mediaPlayer.start();
        mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                mediaPlayer.release();
            }
        });
    }
}
