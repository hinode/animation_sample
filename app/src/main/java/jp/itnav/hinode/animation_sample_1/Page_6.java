package jp.itnav.hinode.animation_sample_1;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.BounceInterpolator;
import android.view.animation.LayoutAnimationController;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;

/**
 * Created by itnav on 2015/02/24.
 */
public class Page_6 extends Activity {

    @Override
    protected void onCreate(Bundle saveInstanceState) {
        super.onCreate(saveInstanceState);

        TranslateAnimation translateAnimation;
        translateAnimation = new TranslateAnimation(Animation.RELATIVE_TO_SELF, 0, Animation.RELATIVE_TO_SELF, 0,
                Animation.RELATIVE_TO_SELF, -6.0f, Animation.RELATIVE_TO_SELF, 0);
        translateAnimation.setDuration(800);
        translateAnimation.setInterpolator(new AccelerateInterpolator());
        translateAnimation.setInterpolator(new BounceInterpolator());

        LayoutAnimationController la = new LayoutAnimationController(translateAnimation);
        la.setDelay(0.5f);


        LinearLayout layout = new LinearLayout(this);
        layout.setOrientation(LinearLayout.HORIZONTAL);
        layout.setLayoutAnimation(la);
        setContentView(layout);

        ImageView imageViewA = new ImageView(this);
        imageViewA.setImageResource(R.drawable.slot_icon3);
        imageViewA.setLayoutParams(new LinearLayout.LayoutParams(100, 100, Gravity.CENTER_VERTICAL));
        layout.addView(imageViewA);


        ImageView imageViewB = new ImageView(this);
        imageViewB.setImageResource(R.drawable.slot_icon3);
        imageViewB.setLayoutParams(new LinearLayout.LayoutParams(100, 100, Gravity.CENTER_VERTICAL));
        layout.addView(imageViewB);

        ImageView imageViewC = new ImageView(this);
        imageViewC.setImageResource(R.drawable.slot_icon3);
        imageViewC.setLayoutParams(new LinearLayout.LayoutParams(100, 100, Gravity.CENTER_VERTICAL));
        layout.addView(imageViewC);


        ImageView imageViewD = new ImageView(this);
        imageViewD.setImageResource(R.drawable.slot_icon3);
        imageViewD.setLayoutParams(new LinearLayout.LayoutParams(100, 100, Gravity.CENTER_VERTICAL));
        layout.addView(imageViewD);

        ImageView imageViewE = new ImageView(this);
        imageViewE.setImageResource(R.drawable.slot_icon3);
        imageViewE.setLayoutParams(new LinearLayout.LayoutParams(100, 100, Gravity.CENTER_VERTICAL));
        layout.addView(imageViewE);


        ImageView imageViewF = new ImageView(this);
        imageViewF.setImageResource(R.drawable.slot_icon3);
        imageViewF.setLayoutParams(new LinearLayout.LayoutParams(100, 100, Gravity.CENTER_VERTICAL));
        layout.addView(imageViewF);
        imageViewF.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Page_6.this, Page_7.class);
                startActivity(intent);
            }
        });
    }
}
